const fs = require("fs");

class Command {
  constructor(name, params) {
    this.name = name;
    this.params = params;
  }
}

class Room {
  constructor(number, guest = new Guest(), keycard = 0) {
    this.number = number;
    this.guest = guest;
    this.keycard = keycard;
  }
}

class Guest {
  constructor(name = "", age = 0) {
    this.name = name;
    this.age = age;
  }
}

function main() {
  const filename = "input.txt";
  const commands = getCommandsFromFileName(filename);
  // Initial hotel
  let hotel = [];

  commands.forEach((command) => {
    switch (command.name) {
      case "create_hotel":
        hotel = initialHotel(command.params);
        return;

      case "book":
        hotel = bookingRoom(hotel, command.params);
        return;

      case "checkout":
        hotel = checkoutRoom(hotel, command.params);
        return;

      case "list_available_rooms":
        findAvailableRooms(hotel);
        return;

      case "list_guest":
        findAllGuest(hotel);
        return;

      case "get_guest_in_room":
        findGuestByRoomNumber(hotel, command.params);
        return;

      case "list_guest_by_age":
        findGuestByAge(hotel, command.params);
        return;

      case "list_guest_by_floor":
        findGuestByFloor(hotel, command.params);
        return;

      case "checkout_guest_by_floor":
        hotel = checkoutGuestByFloor(hotel, command.params);
        return;

      case "book_by_floor":
        hotel = bookingByFloor(hotel, command.params);
        return;

      default:
        return;
    }
  });
}

function getCommandsFromFileName(fileName) {
  const file = fs.readFileSync(fileName, "utf-8");

  return file
    .split("\n")
    .map((line) => line.split(" "))
    .map(
      ([commandName, ...params]) =>
        new Command(
          commandName,
          params.map((param) => {
            const parsedParam = parseInt(param, 10);

            return Number.isNaN(parsedParam) ? param : parsedParam;
          })
        )
    );
}

/**
 * Initial hotel
 * return @hotel
 * @param {floor, roomPerFloor}
 *
 * Create floor and room
 */

function initialHotel(params) {
  const [floor, roomPerFloor] = params;
  let hotel = [];

  // f = floor
  // r = room per floor
  for (let f = 0; f < floor; f++) {
    for (let r = 0; r < roomPerFloor; r++) {
      let number = (f + 1) * 100 + (r + 1);
      let room = new Room(number);

      hotel.push(room);
    }
  }

  console.log(`Hotel created with ${floor} floor(s), ${roomPerFloor} room(s) per floor.`);

  return hotel;
}

/**
 * Booking Room
 * return @hotel
 * @param {hotel}
 * @param {number, name, age}
 *
 */

function bookingRoom(hotel, params) {
  const [number, name, age] = params;
  let guest = new Guest(name, age);
  let cloneHotel = hotel;
  let keycard = generateKeycard(hotel); // initial keycard

  hotel.forEach((room, index) => {
    if (room.number == number) {
      // Room empty
      if (room.guest.name == "") {
        let bookingRoom = new Room(room.number, guest, keycard);
        cloneHotel[index] = bookingRoom;

        console.log(
          `Room ${cloneHotel[index].number} is booked by ${cloneHotel[index].guest.name} with keycard number ${cloneHotel[index].keycard}.`
        );
        return;
      }

      // Room not empty
      console.log(
        `Cannot book room ${room.number} for ${guest.name}, The room is currently booked by ${room.guest.name}.`
      );
      return;
    }
  });

  return cloneHotel;
}

/**
 * Checkout Room
 * return @hotel
 * @param {hotel}
 * @param {keycard, name}
 *
 */

function checkoutRoom(hotel, params) {
  const [keycard, name] = params;
  let cloneHotel = hotel;

  hotel.forEach((room, index) => {
    if (room.keycard == keycard) {
      // Owner checkout
      if (room.guest.name == name) {
        cloneHotel[index] = new Room(room.number);
        console.log(`Room ${cloneHotel[index].number} is checkout.`);
        return;
      }

      // Room not empty
      console.log(`Only ${room.guest.name} can checkout with keycard number ${room.keycard}.`);
      return;
    }
  });

  return cloneHotel;
}

/**
 * Find available rooms
 * no return value
 * @param {hotel}
 *
 */

function findAvailableRooms(hotel) {
  let rooms = hotel.filter((room) => room.keycard == 0);
  rooms = rooms.map((room) => room.number);
  console.log(rooms.toString());
}

/**
 * Find all guests and sort by keycard
 * no return value
 * @param {hotel}
 *
 */

function findAllGuest(hotel) {
  let guests = [];
  hotel = hotel.sort((a, b) => (a.keycard < b.keycard ? -1 : 1)); // Sort by keycards
  hotel.forEach((room) => {
    if (room.guest.name != "" && !guests.includes(room.guest.name)) guests.push(room.guest.name);
  });
  console.log(guests.join(", "));
}

/**
 * Find Guest by room number
 * no return value
 * @param {hotel}
 * @param {number}
 *
 */

function findGuestByRoomNumber(hotel, params) {
  const [number] = params;
  hotel.forEach((room) => {
    if (room.number == number) {
      console.log(room.guest.name);
      return;
    }
  });
}

/**
 * Find Guest by age
 * no return value
 * @param {hotel}
 * @param {operator, age}
 *
 */

function findGuestByAge(hotel, params) {
  const [operator, age] = params;
  let guests = [];
  hotel.forEach((room) => {
    let checkGuestAge = eval(room.guest.age + operator + age);
    let isGuestNotInList = !guests.includes(room.guest.name);
    if (room.guest.name != "" && isGuestNotInList && checkGuestAge) {
      guests.push(room.guest.name);
    }
  });
  console.log(guests.join(", "));
}

/**
 * Find guests by floor
 * no return value
 * @param {hotel}
 * @param {floor}
 *
 */

function findGuestByFloor(hotel, params) {
  const [floor] = params;
  let guests = [];
  hotel.forEach((room) => {
    let isCurrentFloor = checkIsCurrentFloor(room.number, floor);
    let isGuestNotInList = !guests.includes(room.guest.name);
    if (room.guest.name != "" && isGuestNotInList && isCurrentFloor) {
      guests.push(room.guest.name);
    }
  });
  console.log(guests.join(", "));
}

/**
 * Checkout guest by floor
 * return @hotel
 * @param {hotel}
 * @param {floor}
 *
 */

function checkoutGuestByFloor(hotel, params) {
  const [floor] = params;
  let checkoutRooms = [];
  let cloneHotel = hotel;
  hotel.forEach((room, index) => {
    let isCurrentFloor = checkIsCurrentFloor(room.number, floor);
    if (room.guest.name != "" && isCurrentFloor) {
      checkoutRooms.push(room.number);
      cloneHotel[index] = new Room(room.number);
    }
  });
  console.log(`Room ${checkoutRooms.join(", ")} are checkout.`);

  return cloneHotel;
}

/**
 * Booking by floor
 * return @hotel
 * @param {hotel}
 * @param {floor, name, age}
 *
 */

function bookingByFloor(hotel, params) {
  const [floor, name, age] = params;
  // Sort by rooms number
  hotel = hotel.sort((a, b) => (a.number < b.number ? -1 : 1));
  let rooms = [];
  let keycards = [];
  let cloneHotel = hotel;

  hotel.forEach((room, index) => {
    let isCurrentFloor = checkIsCurrentFloor(room.number, floor);

    if (isCurrentFloor) {
      // Check if all room in current floor not empty
      if (room.guest.name != "") {
        console.log(`Cannot book floor ${floor} for ${name}.`);
        rooms = [];
        keycards = [];
        cloneHotel = hotel;
        return;
      }
      let roomNumber = room.number;
      let keycard = generateKeycard(cloneHotel);
      rooms.push(roomNumber);
      keycards.push(keycard);
      cloneHotel[index] = new Room(roomNumber, new Guest(name, age), keycard);
    }
  });

  // Check if can booking all room in current floor
  if (rooms.length != 0) {
    console.log(`Room ${rooms.join(", ")} are booked with keycard number ${keycards.join(", ")}.`);
  }
  return cloneHotel;
}

/**
 * Check room is in current floor
 * return @bool
 * @param {room, floor}
 *
 */

function checkIsCurrentFloor(room, floor) {
  return room.toString()[0] == floor.toString();
}

/**
 * Return new keycard
 * return @int
 * @param {hotel}
 *
 */

function generateKeycard(hotel) {
  let keycards = [...hotel.map((room) => room.keycard)];
  let currentKey = 0;

  // Loop for find hole keycard number in order list
  keycards.forEach(() => {
    if (!keycards.includes(currentKey)) return;
    currentKey += 1;
  });

  return currentKey;
}

/**
 * Running main
 * 6 hour
 */

main();
